/*
 * Copyright 2019-2020 by 
 * 
 * University of Alaska Anchorage
 * College of Engineering
 * Department of Computer Science
 * Anchorage, AK, USA
 *
 * Univ Lyon, CNRS, ENS de Lyon, Inria, Universite Claude Bernard Lyon
 * 1, LIP UMR 5668
 * Lyon, France
 *
 * and by
 * 
 * Universite Paris-Saclay, Univ Paris-Sud, CNRS, Inria, Laboratoire
 * de recherche en informatique,
 * Orsay, France.
 *
 * 
 * IEEE754-2019 Augmented Operations Reference Implementation
 *
 *
 * Contributor: Christoph Quirin Lauter 
 *              (University of Alaska Anchorage) 
 *              christoph.lauter@christoph-lauter.org
 *
 * This software implementation of the IEEE754-209 Augmented
 * Operations is based on the article
 * 
 * S. Boldo, Ch. Lauter, J.-M. Muller: Emulating Round-to-Nearest
 * Ties-to-Zero "Augmented" Floating-Point Operations Using
 * Round-to-Nearest Ties-to-Even Arithmetic
 * 
 * available at
 *
 * https://hal.archives-ouvertes.fr/hal-02137968v3
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

typedef double __binary64_t;

#define BINARY64_PSI                                                          ((0x1.fffffffffffffp-1))
#define BINARY64_OMEGA                                                        ((0x1.fffffffffffffp1023))
#define BINARY64_TWO_MINUS_TWO_TO_MINUS_PREC_PLUS_ONE                         ((0x1.fffffffffffffp0))
#define BINARY64_TWO_TO_EMAX                                                  ((0x1.0p1023))
#define BINARY64_TWO_TO_EMAX_MINUS_PREC_MINUS_ONE                             ((0x1.0p969))
#define BINARY64_TWO_TO_EMIN_PLUS_PREC                                        ((0x1.0p-969))
#define BINARY64_TWO_TO_EMIN_PLUS_ONE_MINUS_TWO_TO_EMIN_MINUS_PREC_PLUS_ONE   ((0x1.fffffffffffffp-1022))
#define BINARY64_TWO_TO_TWO_TIMES_PREC                                        ((0x1.0p106))
#define BINARY64_TWO_TO_EMIN_MINUS_PREC_PLUS_ONE                              ((0x1.0p-1074))
#define BINARY64_TWO_TO_PREC                                                  ((0x1.0p53))
#define BINARY64_TWO_TO_MINUS_PREC                                            ((0x1.0p-53))
#define BINARY64_TWO_TO_EMIN                                                  ((0x1.0p-1022))


static inline __binary64_t __binary64_sign(__binary64_t a) {
  if (!(a == a)) return a;
  if (a == 0.0) return 0.0;
  if (a < 0.0) return -1.0;
  return 1.0;
}

static inline __binary64_t __binary64_fma(__binary64_t a, __binary64_t b, __binary64_t c) {
  return __builtin_fma(a, b, c);
}

static inline __binary64_t __binary64_fabs(__binary64_t a) {
  return __builtin_fabs(a);
}

static inline void __binary64_fast_twosum(__binary64_t * restrict rh, __binary64_t * restrict rl,
					  __binary64_t a, __binary64_t b) {
  __binary64_t resh, resl, t;

  resh = a + b;
  t = resh - a;
  resl = b - t;
  *rh = resh;
  *rl = resl;
}

static inline void __binary64_twomul(__binary64_t * restrict rh, __binary64_t * restrict rl,
				     __binary64_t a, __binary64_t b) {
  __binary64_t resh, resl, t;

  resh = a * b;
  t = -resh;
  resl = __binary64_fma(a, b, t);
  *rh = resh;
  *rl = resl;
}

static inline void __binary64_recomp(__binary64_t * restrict rh, __binary64_t * restrict rl,
				     __binary64_t a, __binary64_t b) {
  __binary64_t resh, resl, z, delta, t;

  z = BINARY64_PSI * a;
  delta = z - a;
  t = b + b;
  if (t == delta) {
    resh = z;
    resl = -b;
  } else {
    resh = a;
    resl = b;
  }
  *rh = resh;
  *rl = resl;
}

static inline void __binary64_aa_simple(__binary64_t * restrict rh, __binary64_t * restrict rl,
					__binary64_t a, __binary64_t b) {
  __binary64_t x, y, ae, be;

  if (__binary64_fabs(b) > __binary64_fabs(a)) {
    x = b;
    y = a;
  } else {
    x = a;
    y = b;
  }
  __binary64_fast_twosum(&ae, &be, x, y);
  __binary64_recomp(rh, rl, ae, be);
}

static inline void __binary64_am_simple(__binary64_t * restrict rh, __binary64_t * restrict rl,
					__binary64_t a, __binary64_t b) {
  __binary64_t ae, be;
  __binary64_twomul(&ae, &be, a, b);
  __binary64_recomp(rh, rl, ae, be);
}

void binary64_augmented_add(__binary64_t *rh, __binary64_t *rl, __binary64_t a, __binary64_t b) {
  __binary64_t x, y, ae, be, az, bz, aep, bep, half;

  if (__binary64_fabs(b) > __binary64_fabs(a)) {
    x = b;
    y = a;
  } else {
    x = a;
    y = b;
  }
  __binary64_fast_twosum(&ae, &be, x, y);
  __binary64_recomp(&az, &bz, ae, be);
  if (bz == 0.0) {
    bz = bz * bz;
    bz = az * bz;
  } else {
    if (__binary64_fabs(ae) > BINARY64_OMEGA) {
      half = 0.5;
      __binary64_fast_twosum(&aep, &bep, half * x, half * y);      
      if (((aep == BINARY64_TWO_TO_EMAX) && (bep == (-BINARY64_TWO_TO_EMAX_MINUS_PREC_MINUS_ONE))) ||
	  ((aep == (-BINARY64_TWO_TO_EMAX)) && (bep == BINARY64_TWO_TO_EMAX_MINUS_PREC_MINUS_ONE))) {
	  az = aep * BINARY64_TWO_MINUS_TWO_TO_MINUS_PREC_PLUS_ONE;
	  bz = -(bep + bep);
      } else {
	  az = ae;
	  bz = ae;
      }
    }
  }
  *rh = az;
  *rl = bz;
}

void binary64_augmented_mul(__binary64_t *resh, __binary64_t *resl, __binary64_t x, __binary64_t y) {
  __binary64_t ae, be, az, bz, mae, xp, aep, bep, t1, t2, t3, z, ap, bp, beta;

  ae = x * y;
  if (__binary64_fabs(ae) > BINARY64_OMEGA) {
    xp = 0.5 * x;
    __binary64_twomul(&aep, &bep, xp, y);
    if (((aep == BINARY64_TWO_TO_EMAX) && (bep == (-BINARY64_TWO_TO_EMAX_MINUS_PREC_MINUS_ONE))) ||
	((aep == (-BINARY64_TWO_TO_EMAX)) && (bep == BINARY64_TWO_TO_EMAX_MINUS_PREC_MINUS_ONE))) {
      az = aep * BINARY64_TWO_MINUS_TWO_TO_MINUS_PREC_PLUS_ONE;
      bz = -(bep + bep);
    } else {
      az = ae;
      bz = ae;
    }
  } else {
    if (__binary64_fabs(ae) <= BINARY64_TWO_TO_EMIN_PLUS_PREC) {
      if (ae == 0.0) {
	az = ae;
	bz = ae;
      } else {
	if (__binary64_fabs(ae) <= BINARY64_TWO_TO_EMIN_PLUS_ONE_MINUS_TWO_TO_EMIN_MINUS_PREC_PLUS_ONE) {
	  bz = 0.0; bz *= bz;
	  __binary64_twomul(&t1, &t2, (x * BINARY64_TWO_TO_TWO_TIMES_PREC), y);
	  t3 = __binary64_fma(-ae, BINARY64_TWO_TO_TWO_TIMES_PREC, t1);
	  z = t2 + t3;
	  if ((z == (-(__binary64_sign(ae) * BINARY64_TWO_TO_EMIN_PLUS_PREC))) &&
	      ((z - t3) == t2)) {
	    az = ae - __binary64_sign(ae) * BINARY64_TWO_TO_EMIN_MINUS_PREC_PLUS_ONE;
	    bz = bz * ae;
	  } else {
	    az = ae;
	    if (z == 0.0) {
	      bz = bz * az;
	    } else {
	      bz = bz * z;
	    }
	  }
	} else {
	  __binary64_am_simple(&ap, &bp, (BINARY64_TWO_TO_PREC * x), y);
	  az = BINARY64_TWO_TO_MINUS_PREC * ap;
	  beta = BINARY64_TWO_TO_MINUS_PREC * bp;
	  if (__binary64_fma(BINARY64_TWO_TO_PREC, beta, -bp) == __binary64_sign(beta) * BINARY64_TWO_TO_EMIN) {
	    bz = beta - __binary64_sign(beta) * BINARY64_TWO_TO_EMIN_MINUS_PREC_PLUS_ONE;
	  } else {
	    bz = beta;
	  }
	  if (bz == 0.0) {
	    bz = bz * bz;
	    if (bp == 0.0) {
	      bz = bz * az;
	    } else {
	      bz = bz * bp;
	    }
	  }
	}
      }
    } else {
      mae = -ae;
      be = __binary64_fma(x, y, mae);
      __binary64_recomp(&az, &bz, ae, be);
      if (bz == 0.0) {
	bz = bz * bz;
	bz = bz * az;
      }
    }
  }
  *resh = az;
  *resl = bz;
}


