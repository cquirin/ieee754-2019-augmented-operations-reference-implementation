/*
 * Copyright 2019-2020 by 
 * 
 * University of Alaska Anchorage
 * College of Engineering
 * Department of Computer Science
 * Anchorage, AK, USA
 *
 * Univ Lyon, CNRS, ENS de Lyon, Inria, Universite Claude Bernard Lyon
 * 1, LIP UMR 5668
 * Lyon, France
 *
 * and by
 * 
 * Universite Paris-Saclay, Univ Paris-Sud, CNRS, Inria, Laboratoire
 * de recherche en informatique,
 * Orsay, France.
 *
 * 
 * IEEE754-2019 Augmented Operations Reference Implementation
 *
 *
 * Contributor: Christoph Quirin Lauter 
 *              (University of Alaska Anchorage) 
 *              christoph.lauter@christoph-lauter.org
 *
 * This software implementation of the IEEE754-209 Augmented
 * Operations is based on the article
 * 
 * S. Boldo, Ch. Lauter, J.-M. Muller: Emulating Round-to-Nearest
 * Ties-to-Zero "Augmented" Floating-Point Operations Using
 * Round-to-Nearest Ties-to-Even Arithmetic
 * 
 * available at
 *
 * https://hal.archives-ouvertes.fr/hal-02137968v3
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */


#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fenv.h>

#define INITIAL_LINE_ALLOC                  (((size_t) (1024)))
#define PARSE_BINARY64_STATIC_BUFFER_SIZE   (((size_t) (1024)))

typedef double __binary64_t;

typedef union {
  __binary64_t f;
  uint64_t     i;
} __binary64_conv_t;

enum __flags_enum_t {
  FLAG_INVALID          = 1,
  FLAG_UNDERFLOW        = 2,
  FLAG_OVERFLOW         = 4,
  FLAG_DIVISION_BY_ZERO = 8,
  FLAG_PRECISION        = 16
};
typedef enum __flags_enum_t __flags_t;

#define FLAG_ALL_FLAGS    ((FLAG_INVALID | FLAG_UNDERFLOW | FLAG_OVERFLOW | FLAG_DIVISION_BY_ZERO | FLAG_PRECISION))

enum __operation_enum_t {
  AUGMENTED_ADD,
  AUGMENTED_MUL
};
typedef enum __operation_enum_t __operation_t;

struct __test_entry_struct_t {
  __operation_t op;
  __binary64_t  x;
  __binary64_t  y;
  __binary64_t  a;
  __binary64_t  b;
  __flags_t     inflags;
  __flags_t     outflags;
  __binary64_t  actual_a;
  __binary64_t  actual_b;
  __flags_t     actual_outflags;
};
typedef struct __test_entry_struct_t __test_entry_t;

enum __parse_result_enum_t {
  PARSE_COMMENT,
  PARSE_OKAY,
  PARSE_ERROR
};
typedef enum __parse_result_enum_t __parse_result_t;

void binary64_augmented_add(__binary64_t *, __binary64_t *, __binary64_t, __binary64_t);
void binary64_augmented_mul(__binary64_t *, __binary64_t *, __binary64_t, __binary64_t);

char *find_next_word(char *str) {
  char *curr;

  for (curr=str;*curr!='\0';curr++) {
    switch(*curr) {
    case ' ':
    case '\t':
    case '\n':
      break;
    default:
      return curr;
    }
  }
  return curr;
}

char *find_next_space(char *str) {
  char *curr;

  for (curr=str;*curr!='\0';curr++) {
    switch(*curr) {
    case ' ':
    case '\t':
    case '\n':
      return curr;
      break;
    default:
      break;
    }
  }
  return curr;
}

int parse_operation(__operation_t *res, char *str, size_t len) {
  if (strncmp("add", str, len) == 0) {
    *res = AUGMENTED_ADD;
    return 1;
  }
  if (strncmp("mul", str, len) == 0) {
    *res = AUGMENTED_MUL;
    return 1;
  }  
  return 0;
}

int __parse_binary64_hexadecimal_memory(__binary64_t *res, char *str) {
  size_t len;
  int i;
  uint64_t t, d;
  __binary64_conv_t c;

  len = strlen(str);
  if (len != ((size_t) 18)) return 0;
  if (str[0] != '0') return 0;
  if (str[1] != 'x') return 0;
  for (i=2;i<18;i++) {
    if (!((('0' <= str[i]) && (str[i] <= '9')) ||
	  (('a' <= str[i]) && (str[i] <= 'f')) ||
	  (('A' <= str[i]) && (str[i] <= 'F')))) return 0;
  }
  t = 0;
  for (i=2;i<18;i++) {
    if (('0' <= str[i]) && (str[i] <= '9')) {
      d = (uint64_t) (str[i] - '0');
    } else {
      if (('a' <= str[i]) && (str[i] <= 'f')) {
	d = ((uint64_t) (str[i] - 'a')) + ((uint64_t) 0xa);
      } else {
	if (('A' <= str[i]) && (str[i] <= 'F')) {
	  d = ((uint64_t) (str[i] - 'A')) + ((uint64_t) 0xa);
	} else {
	  return 0;
	}
      }
    }
    t = (t << 4) | d;
  }
  c.i = t;
  *res = c.f;
  return 1;
}

int __parse_binary64_inner(__binary64_t *res, char *str) {
  __binary64_t r;
  char *endptr;

  r = *res;
  if (__parse_binary64_hexadecimal_memory(&r, str)) {
    *res = r;
    return 1;
  }
  if (*str == '\0') return 0;
  errno = 0;
  r = strtod(str, &endptr);
  if ((errno != 0) || (*endptr != '\0')) {
    return 0;
  }
  *res = r;
  return 1;
}

int parse_binary64(__binary64_t *res, char *str, size_t len) {
  char buffer[PARSE_BINARY64_STATIC_BUFFER_SIZE];
  int freeMem;
  char *s;
  __binary64_t r;
  int okay;
  
  if (len == ((size_t) 0)) return 0;
  freeMem = 0;
  if (len + 1 <= PARSE_BINARY64_STATIC_BUFFER_SIZE) {
    memset(buffer, 0, PARSE_BINARY64_STATIC_BUFFER_SIZE);
    s = buffer;
  } else {
    freeMem = 1;
    if ((s = ((char *) calloc(len + 1, sizeof(char)))) == NULL) {
      fprintf(stderr, "Not enough memory.\n");
      return 0;
    }
  }
  memmove(s, str, len);
  r = *res;
  okay = __parse_binary64_inner(&r, s);
  if (freeMem) {
    free(s);
  }
  if (okay) {
    *res = r; 
  }
  return okay;
}

int parse_flags(__flags_t *res, char *str, size_t len) {
  int okay;
  __flags_t rp, rn;
  size_t i;

  okay = 1;
  rp = 0;
  rn = 0;
  for (i=0;(i<len)&&okay;i++) {
    switch (str[i]) {
    case 'i':
      rn |= FLAG_INVALID;
      break;
    case 'u':
      rn |= FLAG_UNDERFLOW;
      break;
    case 'o':
      rn |= FLAG_OVERFLOW;
      break;
    case 'z':
      rn |= FLAG_DIVISION_BY_ZERO;
      break;
    case 'p':
      rn |= FLAG_PRECISION;
      break;
    case 'I':
      rp |= FLAG_INVALID;
      break;
    case 'U':
      rp |= FLAG_UNDERFLOW;
      break;
    case 'O':
      rp |= FLAG_OVERFLOW;
      break;
    case 'Z':
      rp |= FLAG_DIVISION_BY_ZERO;
      break;
    case 'P':
      rp |= FLAG_PRECISION;
      break;
    default:
      okay = 0;
      break;
    }
  }
  if (okay) {
    if ((rp & rn) != 0) {
      okay = 0;
    }
  }
  if (okay) {
    *res = rp;
  }
  return okay;
}

__parse_result_t parse_word(__test_entry_t *entry, int w, char *str, size_t len) {

  /* Stupid empty input */
  if (len < 1) return PARSE_ERROR;
  
  /* Handle comment lines */
  if ((w == 0) && (len >= 2) && (str[0] == '/') && (str[1] == '/')) return PARSE_COMMENT;

  /* Handle the different input and output values */
  switch (w) {
  case 0:
    if (parse_operation(&entry->op, str, len))
      return PARSE_OKAY;
    break;    
  case 1:
    if (parse_binary64(&entry->x, str, len))
      return PARSE_OKAY;
    break;
  case 2:
    if (parse_binary64(&entry->y, str, len))
      return PARSE_OKAY;
    break;
  case 3:
    if (parse_flags(&entry->inflags, str, len))
      return PARSE_OKAY;
    break;
  case 4:
    if (parse_binary64(&entry->a, str, len))
      return PARSE_OKAY;
    break;
  case 5:
    if (parse_binary64(&entry->b, str, len))
      return PARSE_OKAY;
    break;
  case 6:
    if (parse_flags(&entry->outflags, str, len))
      return PARSE_OKAY;
    break;
  default:
    return PARSE_ERROR;
    break;
  }

  /* Default reaction: parse error */
  return PARSE_ERROR;
}

void set_flags(__flags_t flags) {
  int excepts;

  excepts = 0;
  if ((flags & FLAG_PRECISION) != 0)        excepts |= FE_INEXACT;
  if ((flags & FLAG_INVALID) != 0)          excepts |= FE_INVALID;
  if ((flags & FLAG_UNDERFLOW) != 0)        excepts |= FE_UNDERFLOW;
  if ((flags & FLAG_OVERFLOW) != 0)         excepts |= FE_OVERFLOW;
  if ((flags & FLAG_DIVISION_BY_ZERO) != 0) excepts |= FE_DIVBYZERO;
  feclearexcept(FE_ALL_EXCEPT);
  feraiseexcept(excepts);
}

__flags_t get_flags() {
  __flags_t res;
  int excepts;

  excepts = fetestexcept(FE_ALL_EXCEPT);
  res = 0;
  if ((excepts & FE_INEXACT) != 0)   res |= FLAG_PRECISION;
  if ((excepts & FE_INVALID) != 0)   res |= FLAG_INVALID;
  if ((excepts & FE_UNDERFLOW) != 0) res |= FLAG_UNDERFLOW;
  if ((excepts & FE_OVERFLOW) != 0)  res |= FLAG_OVERFLOW;
  if ((excepts & FE_DIVBYZERO) != 0) res |= FLAG_DIVISION_BY_ZERO;
  return res;
}

__binary64_t binary64_quietize_nan(__binary64_t x) {
  __binary64_conv_t c;
  
  c.f = x;
  if (((c.i & ((uint64_t) 0x7ff000000000000ull)) == ((uint64_t) 0x7ff000000000000ull)) &&
      ((c.i & ((uint64_t) 0x000ffffffffffffull)) != ((uint64_t) 0x0ull))) {
    c.i |= (uint64_t) 0x000800000000000ull;
    return c.f;
  }
  return x;
}

__binary64_t binary64_get_generic_nan() {
  __flags_t flags;
  volatile __binary64_t a, b, c;

  a = 0.0;
  flags = get_flags();
  b = 0.0;
  c = a / b;
  set_flags(flags);
  return c;
}

/* Compare a with b, if any is a NaN, it needs to be one of x or y, or a generic NaN */
int compare_binary64(__binary64_t a, __binary64_t b, __binary64_t x, __binary64_t y) {
  __binary64_conv_t ca, cb, cx, cy, ct;

  ca.f = a;
  cb.f = b;
  if (ca.i == cb.i) return 1;
  if ((((ca.i & ((uint64_t) 0x7ff000000000000ull)) == ((uint64_t) 0x7ff000000000000ull)) &&
       ((ca.i & ((uint64_t) 0x000ffffffffffffull)) != ((uint64_t) 0x0ull))) &&
      (((cb.i & ((uint64_t) 0x7ff000000000000ull)) == ((uint64_t) 0x7ff000000000000ull)) &&
       ((cb.i & ((uint64_t) 0x000ffffffffffffull)) != ((uint64_t) 0x0ull)))) {
    /* Both a and b are NaN but not the same NaN */
    cx.f = binary64_quietize_nan(x);
    cy.f = binary64_quietize_nan(y);
    if (((cx.i & ((uint64_t) 0x7ff000000000000ull)) == ((uint64_t) 0x7ff000000000000ull)) &&
	((cx.i & ((uint64_t) 0x000ffffffffffffull)) != ((uint64_t) 0x0ull))) {
      if (((cy.i & ((uint64_t) 0x7ff000000000000ull)) == ((uint64_t) 0x7ff000000000000ull)) &&
	  ((cy.i & ((uint64_t) 0x000ffffffffffffull)) != ((uint64_t) 0x0ull))) {
	/* Both x and y are NaN */
	if (ca.i == cx.i) return 1;
	if (ca.i == cy.i) return 1;
	return 0;
      } else {
	/* Only x is NaN */
	return (ca.i == cx.i);
      }
    } else {
      if (((cy.i & ((uint64_t) 0x7ff000000000000ull)) == ((uint64_t) 0x7ff000000000000ull)) &&
	  ((cy.i & ((uint64_t) 0x000ffffffffffffull)) != ((uint64_t) 0x0ull))) {
	/* Only y is NaN */
	return (ca.i == cy.i);	
      } else {
	/* None of x or y is NaN */
	ct.f = binary64_get_generic_nan();
	return (ca.i == ct.i);	
      }
    }
  }
  return 0;
}

int compare_flags(__flags_t a, __flags_t b) {
  return (((a & FLAG_ALL_FLAGS) ^ (b & FLAG_ALL_FLAGS)) == ((__flags_t) 0));
}

int check_test_entry(__test_entry_t *entry, int ignoreFlags) { 
  if (!compare_binary64(entry->actual_a, entry->a, entry->x, entry->y)) return 0;
  if (!compare_binary64(entry->actual_b, entry->b, entry->x, entry->y)) return 0;
  if (!ignoreFlags) {
    if (!compare_flags(entry->actual_outflags, entry->outflags)) return 0;
  }
  /* Add a check that the same NaN is produced on invalid inputs if
     none of the inputs is a NaN 
  */
  return 1;  
}

int run_test_entry(__test_entry_t *entry, int ignoreFlags) {
  int res;
  __flags_t flags;
  __binary64_t a, b;

  flags = get_flags();

  set_flags(entry->inflags);
  switch (entry->op) {
  case AUGMENTED_ADD:
    binary64_augmented_add(&a, &b, entry->x, entry->y);
    entry->actual_a = a;
    entry->actual_b = b;
    break;
  case AUGMENTED_MUL:
    binary64_augmented_mul(&a, &b, entry->x, entry->y);
    entry->actual_a = a;
    entry->actual_b = b;
    break;
  default:
    set_flags(flags);
    return 0;
    break;
  }
  entry->actual_outflags = get_flags();
  
  res = check_test_entry(entry, ignoreFlags);
  
  set_flags(flags);
  
  return res;
}

void print_operation(__operation_t op) {
  switch (op) {
  case AUGMENTED_ADD:
    printf("add");
    break;
  case AUGMENTED_MUL:
    printf("mul");
    break;
  default:
    printf("unknown");
    break;
  }
}

void print_binary64(__binary64_t x) {
  __binary64_conv_t c;
  c.f = x;
  printf("0x%016llx", (unsigned long long int) c.i);
}

void print_flags(__flags_t flags) {
  if ((flags & FLAG_PRECISION) != 0)        { printf("P"); } else { printf("p"); }
  if ((flags & FLAG_INVALID) != 0)          { printf("I"); } else { printf("i"); }
  if ((flags & FLAG_UNDERFLOW) != 0)        { printf("U"); } else { printf("u"); }
  if ((flags & FLAG_OVERFLOW) != 0)         { printf("O"); } else { printf("o"); }
  if ((flags & FLAG_DIVISION_BY_ZERO) != 0) { printf("Z"); } else { printf("z"); }
}

void print_test_entry(__test_entry_t *entry) {
  print_operation(entry->op);
  printf(" ");
  print_binary64(entry->x);
  printf(" ");
  print_binary64(entry->y);
  printf(" ");
  print_flags(entry->inflags);
  printf(" ");
  print_binary64(entry->a);
  printf(" ");
  print_binary64(entry->b);
  printf(" ");
  print_flags(entry->outflags);
  printf(" Got: ");
  print_binary64(entry->actual_a);
  printf(" ");
  print_binary64(entry->actual_b);
  printf(" ");
  print_flags(entry->actual_outflags);
  printf("\n");
}


int run_test_line(char *str, int ignoreFlags, int *tests) {
  char *curr, *next;
  size_t len;
  int okay, do_test;
  int word;
  __test_entry_t test_entry;

  /* Reset test entry */
  memset(&test_entry, 0, sizeof(__test_entry_t));

  /* Start pessimistic */
  okay = 0;
  do_test = 1;
  
  /* Find first word */
  curr = find_next_word(str);
  next = find_next_space(curr);
  
  /* Word for word */
  word = 0;
  while ((*curr != '\0') && (next > curr)) {
    len = (size_t) (next - curr);
    switch (parse_word(&test_entry, word, curr, len)) {
    case PARSE_COMMENT:
      return 1;
      break;
    case PARSE_OKAY:
      break;
    default:
      do_test = 0;
      break;
    }
    if (!do_test) {
      break;
    }
    curr = find_next_word(next);
    next = find_next_space(curr);
    word++;
  }

  if (do_test) {
    (*tests)++;
    okay = run_test_entry(&test_entry, ignoreFlags);
    if (!okay) {
      print_test_entry(&test_entry);
    }
  }

  /* Return verdict */
  return okay;
}

int run_test_file(FILE *fd, int ignoreFlags, int *tests) {
  int t;
  int okay, ok;
  char c;
  char *line, *temp;
  size_t line_alloc, line_len;

  *tests = 0;
  if ((line = ((char *) calloc(INITIAL_LINE_ALLOC, sizeof(char)))) == NULL) {
    fprintf(stderr, "Not enough memory.\n");
    return 0;
  }
  line_alloc = INITIAL_LINE_ALLOC;
  okay = 1;
  line_len = 0;
  errno = 0;
  while ((t = fgetc(fd)) != EOF) {
    if (errno != 0) {
      okay = 0;
      break;
    }
    if (t < 0) {
      okay = 0;
      break;
    }
    c = (char) t;
    if (c != '\0') {
      if (c == '\n') {
	if ((line_len > 0) && (line[0] != '\0')) {
	  ok = run_test_line(line, ignoreFlags, tests);
	} else {
	  ok = 1;
	}
	memset(line, 0, line_len);
	line_len = 0;
	okay = okay && ok;
      } else {
	if (line_len > line_alloc - 3) {
	  temp = (char *) realloc(line, line_alloc * 2);
	  if (temp == NULL) {
	    free(line);
	    fprintf(stderr, "Not enough memory.\n");
	    return 0;
	  }
	  line_alloc *= 2;
	  line = temp;
	}
	line[line_len] = c;
	line_len++;
      }
    }
    errno = 0;
  }
  if (errno != 0) {
    okay = 0;
  }
  free(line);
  return okay;
}

int main(int argc, char **argv) {
  FILE *fd;
  int res;
  int ignoreFlags;
  char *filename;
  int i;
  int tests;
  
  if (argc < 2) {
    fprintf(stderr, "Not enough arguments.\n");
    return 1;
  }
  ignoreFlags = 0;
  filename = NULL;
  for (i=1;i<argc;i++) {
    if (strcmp(argv[i], "--ignoreFlags") == 0) {
      ignoreFlags = 1;
    } else {
      if (filename == NULL) {
	filename = argv[i];
      } else {
	fprintf(stderr, "Too many filenames given.\n");
	return 1;
      }
    }
  }
  if (filename == NULL) {
    fprintf(stderr, "No filename given.\n");
    return 1;
  }  
  fd = fopen(filename, "r");
  if (fd == NULL) {
    fprintf(stderr, "Could not open file %s: %s\n", filename, strerror(errno));
    return 1;
  }

  tests = 0;
  res = run_test_file(fd, ignoreFlags, &tests);
  
  if (fclose(fd) < 0) {
    fprintf(stderr, "Could not close file %s: %s\n", filename, strerror(errno));
    return 1;
  }

  if (res) {
    printf("No errors have been detected. %d tests have been run.\n", tests);
  } else {
    printf("There have been errors detected. %d tests have been run.\n", tests);
  }
  
  if (res) return 0;
  return 1;
}

