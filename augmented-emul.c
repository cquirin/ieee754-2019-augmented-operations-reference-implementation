/*
 * Copyright 2019-2020 by 
 * 
 * University of Alaska Anchorage
 * College of Engineering
 * Department of Computer Science
 * Anchorage, AK, USA
 *
 * Univ Lyon, CNRS, ENS de Lyon, Inria, Universite Claude Bernard Lyon
 * 1, LIP UMR 5668
 * Lyon, France
 *
 * and by
 * 
 * Universite Paris-Saclay, Univ Paris-Sud, CNRS, Inria, Laboratoire
 * de recherche en informatique,
 * Orsay, France.
 *
 * 
 * IEEE754-2019 Augmented Operations Reference Implementation
 *
 *
 * Contributor: Christoph Quirin Lauter 
 *              (University of Alaska Anchorage) 
 *              christoph.lauter@christoph-lauter.org
 *
 * This software implementation of the IEEE754-209 Augmented
 * Operations is based on the article
 * 
 * S. Boldo, Ch. Lauter, J.-M. Muller: Emulating Round-to-Nearest
 * Ties-to-Zero "Augmented" Floating-Point Operations Using
 * Round-to-Nearest Ties-to-Even Arithmetic
 * 
 * available at
 *
 * https://hal.archives-ouvertes.fr/hal-02137968v3
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include <stdint.h>

#ifdef AUGMENTED_EMULATION_AS_REFERENCE

#define BINARY64_AUGMENTED_ADD  __ref_binary64_augmented_add
#define BINARY64_AUGMENTED_MUL  __ref_binary64_augmented_mul

#else

#define BINARY64_AUGMENTED_ADD  binary64_augmented_add
#define BINARY64_AUGMENTED_MUL  binary64_augmented_mul

#endif 


typedef double __binary64_t;

#define BINARY64_MANTISSA_LENGTH       ((52))
#define BINARY64_EXPONENT_WIDTH        ((11))
#define BINARY64_EXPONENT_BIAS         (((int64_t) ((((uint64_t) 1) << (BINARY64_EXPONENT_WIDTH - 1)) - ((uint64_t) 1))))

typedef union {
  __binary64_t f;
  uint64_t     i;
} __binary64_conv_t;

typedef enum {
  FP_CLASS_NUMBER,
  FP_CLASS_ZERO,
  FP_CLASS_INFINITY,
  FP_CLASS_NAN
} __fp_class_t;

typedef enum {
  FP_STATUS_PRECISION = 1,
  FP_STATUS_OVERFLOW = 2,
  FP_STATUS_UNDERFLOW = 4
} __fp_status_t;


static inline unsigned int __lzc64(uint64_t a) {
  unsigned int r;
  uint64_t t;
  if (a == ((uint64_t) 0)) return (unsigned int) 64;
  if (sizeof(uint64_t) == sizeof(unsigned int)) {
    return (unsigned int) __builtin_clz(a);
  } else {
    if (sizeof(uint64_t) == sizeof(unsigned long)) {
      return (unsigned int) __builtin_clzl(a);
    } else {
      if (sizeof(uint64_t) == sizeof(unsigned long long)) {
        return (unsigned int) __builtin_clzll(a);
      } else {
        for (r=0, t=a; ((t & (((uint64_t) 1) << 63)) == ((uint32_t) 0)); r++, t<<=1);
        return r;
      }
    }
  }
  return (unsigned int) 0;
}

static inline unsigned int __lzc128(__uint128_t a) {
  uint64_t ah, al;

  ah = (uint64_t) (a >> 64);
  al = (uint64_t) a;
  if (ah == ((uint64_t) 0)) {
    return ((unsigned int) 64) + __lzc64(al);
  }
  return __lzc64(ah);
}

static inline uint64_t __binary64_decode(__binary64_t x) {
  __binary64_conv_t xdb;

  xdb.f = x;

  return xdb.i;
}

static inline __binary64_t __binary64_encode(uint64_t x) {
  __binary64_conv_t xdb;

  xdb.i = x;

  return xdb.f;
}


/* Decompose a binary64 FP number

   Return the FP class of the number.

   If x is a finite, non-zero number, ensure that

   (-1)^s * 2^E * m = x,

   with 2^(k - 1) <= m <= 2^k - 1

   where k = 53.

*/
static inline __fp_class_t __binary64_decompose(int *s, int32_t *E, uint64_t *m, __binary64_t x) {
  uint64_t t, mm, ee, ss;
  unsigned int lzc;

  /* Get IEEE754 memory representation */
  t = __binary64_decode(x);

  /* Decompose into sign, biased exponent and mantissa (without hidden bit) */
  mm = t & ((((uint64_t) 1) << BINARY64_MANTISSA_LENGTH) - ((uint64_t) 1));
  ee = (t >> BINARY64_MANTISSA_LENGTH) & ((((uint64_t) 1) << (BINARY64_EXPONENT_WIDTH)) - ((uint64_t) 1));
  ss = (t >> (BINARY64_MANTISSA_LENGTH + BINARY64_EXPONENT_WIDTH));

  /* Check for inf and NaN */
  if ((ee & ((((uint64_t) 1) << (BINARY64_EXPONENT_WIDTH)) - ((uint64_t) 1))) ==
      ((((uint64_t) 1) << (BINARY64_EXPONENT_WIDTH)) - ((uint64_t) 1))) {
    if (mm == ((uint64_t) 0)) {
      /* Infinity */
      *s = !!((int) ss);
      return FP_CLASS_INFINITY;
    }
    /* NaN */
    return FP_CLASS_NAN;
  }

  /* Check for subnormals and zero */
  if (ee == ((uint64_t) 0)) {
    if (mm == ((uint64_t) 0)) {
      /* Zero */
      *s = !!((int) ss);
      *E = (int32_t) 0;
      *m = (uint64_t) 0;
      return FP_CLASS_ZERO;
    }
    /* Subnormal */
    lzc = __lzc64(mm);
    *s = !!((int) ss);
    *E = (int32_t) ((((int64_t) 1) - ((int64_t) BINARY64_EXPONENT_BIAS) - ((int64_t) BINARY64_MANTISSA_LENGTH)) -
		    ((int64_t) ((((int) lzc) - 64 + 1) + BINARY64_MANTISSA_LENGTH)));
    *m = mm << ((((int) lzc) - 64 + 1) + BINARY64_MANTISSA_LENGTH);
    return FP_CLASS_NUMBER;
  }

  /* Normal */
  *s = !!((int) ss);
  *E = (int32_t) (((int64_t) ee) - ((int64_t) (BINARY64_EXPONENT_BIAS + BINARY64_MANTISSA_LENGTH)));
  *m = mm | (((uint64_t) 1) << BINARY64_MANTISSA_LENGTH);
  return FP_CLASS_NUMBER;
}

/* Return (-1)^s * 2^E * m

   rounded to the nearest binary64 FP number, breaking ties toward zero.

   If m is zero, disregard E and return an FP number properly signed
   according to s.

   On overflow, return a properly signed infinity.

   On gradual or complete underflow, return a subnormal number or a
   properly signed zero.

   This function never generates a NaN.

   This function does not signal any exceptions.

*/
static inline __binary64_t __binary64_round_to_nearest_ties_toward_zero(int s, int32_t E, uint64_t m, __fp_status_t *status) {
  uint64_t t, mm, ee, imm;
  unsigned int lzc;
  int32_t EE;
  unsigned int sigma;
  int inexact;

  /* Handle case of m being zero */
  if (m == ((uint64_t) 0)) {
    *status = (__fp_status_t) 0;
    t = ((uint64_t) (!!s)) << (BINARY64_MANTISSA_LENGTH + BINARY64_EXPONENT_WIDTH);
    return __binary64_encode(t);
  }

  /* Here, m is not zero

     Normalize m.

  */
  lzc = __lzc64(m);
  mm = m << lzc;
  EE = E - ((int32_t) lzc);

  /* Now 2^63 <= mm <= 2^64 - 1

     Decide between normal and subnormal rounding.

  */
  if (((int64_t) EE) < ((-((int64_t) BINARY64_EXPONENT_BIAS) +
			 ((int64_t) 1)) - ((int64_t) (64 - 1)))) {
    /* Perform subnormal rounding

       The result can still be a normal due to rounding.

       The result can also be completely underflowed, in which case a
       properly signed zero needs to be returned.

    */

    /* Make room for a carry bit

       Using round to odd in order to maintain the correctness of the
       subsequent rounding to nearest.
    */
    mm = (mm >> 1) | (mm & ((uint64_t) 1));
    EE++;

    /* Denormalize mm according to EE

       The denormalization essentially is a truncation. In order
       not to affect the subsequent rounding to nearest, we need to
       change that truncation to a rounding to odd.

    */
    sigma = (unsigned int) (((int32_t) 1) - ((int32_t) BINARY64_EXPONENT_BIAS) -
                            ((int32_t) (64 - 1 - 1)) - EE);
    if (sigma > ((unsigned int) (64 - 1))) {
      sigma = (unsigned int) (64 - 1);
    }
    mm = (mm >> sigma) |
      ((uint64_t) (!!((mm & ((((uint64_t) 1) << sigma) - ((uint64_t) 1))) != ((uint64_t) 0))));

    /* Keep the unrounded intermediate result */
    imm = mm;
    
    /* Add in predecessor of half an ulp */
    mm += (((uint64_t) 1) << (((64 - 1 - 1) - BINARY64_MANTISSA_LENGTH) - 1)) - ((uint64_t) 1);

    /* Round toward zero, which results in round-to-nearest ties toward zero */
    mm >>= ((64 - 1 - 1) - BINARY64_MANTISSA_LENGTH);

    /* Construct properly signed subnormal, zero or normal */
    t = (((uint64_t) (!!s)) << (BINARY64_MANTISSA_LENGTH + BINARY64_EXPONENT_WIDTH)) | mm;

    /* Set the appropriate status */
    *status = FP_STATUS_UNDERFLOW;
    if (imm != (mm << ((64 - 1 - 1) - BINARY64_MANTISSA_LENGTH))) {
      *status |= FP_STATUS_PRECISION;
    }
    
    /* Return the encoded result */
    return __binary64_encode(t);
  }

  /* Perform normal rounding */

  /* Make room for a carry bit

     Using round to odd in order to maintain the correctness of the
     subsequent rounding to nearest.
  */
  mm = (mm >> 1) | (mm & ((uint64_t) 1));
  EE++;

  /* Keep the unrounded intermediate result */
  imm = mm;

  /* Add in predecessor of half an ulp */
  mm += (((uint64_t) 1) << (((64 - 1 - 1) - BINARY64_MANTISSA_LENGTH) - 1)) - ((uint64_t) 1);

  /* Round toward zero, which results in round-to-nearest ties toward zero */
  mm >>= ((64 - 1 - 1) - BINARY64_MANTISSA_LENGTH);  
  EE += (int32_t) ((64 - 1 - 1) - BINARY64_MANTISSA_LENGTH);

  /* Compute inexact flag */
  inexact = (imm != (mm << ((64 - 1 - 1) - BINARY64_MANTISSA_LENGTH)));
  
  /* Check for exponent change after rounding */
  if (mm == (((uint64_t) 1) << (BINARY64_MANTISSA_LENGTH + 1))) {
    mm >>= 1;
    EE++;
  }
  /* Go from mantissa between 2^(k-1) and 2^k - 1 to mantissa between 1 and 2 */
  EE += (int32_t) BINARY64_MANTISSA_LENGTH;
  /* Bias the exponent */
  ee = (uint64_t) (((int64_t) EE) + ((int64_t) BINARY64_EXPONENT_BIAS));
  /* Check for overflow of biased exponent */
  if (ee >= ((((uint64_t) 1) << BINARY64_EXPONENT_WIDTH) - ((uint64_t) 1))) {
    /* Overflow

       Return properly signed infinity.
    */
    t = (((uint64_t) (!!s)) << (BINARY64_MANTISSA_LENGTH + BINARY64_EXPONENT_WIDTH)) |
      (((((uint64_t) 1) << BINARY64_EXPONENT_WIDTH) - ((uint64_t) 1)) << BINARY64_MANTISSA_LENGTH);

    /* Set appropriate status */
    *status = FP_STATUS_OVERFLOW | FP_STATUS_PRECISION;

    /* Return encoded result */
    return __binary64_encode(t);
  }
  /* Normal result

     Hide hidden bit.
  */
  mm &= (((uint64_t) 1) << BINARY64_MANTISSA_LENGTH) - ((uint64_t) 1);
  /* Construct FP number */
  t = (((uint64_t) (!!s)) << (BINARY64_MANTISSA_LENGTH + BINARY64_EXPONENT_WIDTH)) |
    ((ee << BINARY64_MANTISSA_LENGTH) | mm);

  /* Set appropriate status */
  if (inexact) {
    *status = FP_STATUS_PRECISION;
  } else {
    *status = (__fp_status_t) 0;
  }

  /* Return encoded result */
  return __binary64_encode(t);
}


/* Return (-1)^s * 2^E * m

   as a pair of binary64 FP numbers rh + rl.

   There are several possible cases:

   (i)   Base case:

         *  rh is the rounding to nearest ties to zero of

            (-1)^s * 2^E * m

            and that rounding does not overflow

         *  rl is the rounding to nearest ties to zero of

            (-1)^s * 2^E * m - rh.

         This case also subsumes the case when the rounding
         for rl underflows, or when the rounding for rh
         underflows gradually but not completely.

         In addition, this case also covers the situation
         when

         (-1)^s * 2^E * m

         is exactly representable on rh, i.e. no rounding
         occurs on the computation of rh. In this case,

         * rl is set to zero with the sign of
           the one of rh, which is not zero.

   (ii)  Complete underflow case:

         * rh is set to zero with the sign of

           (-1)^s * 2^E * m

           when the rounding to nearest ties to zero of
           this quantity underflows completely

         * rl is set to zero with the same sign as
           the one of rh.

   (iii) Zero case: when m is zero,

         * rh is set to zero with s as its sign
         * rl is set to zero with the same sign as
           the one of rh.

         This case (iii) and the previous case (ii) can
         actually be merged with respect to the logic
         to obtain the correct sign of rl.

   (iv)  Overflow case: 

         * rh is set to infinity, with the sign of
	  
	   (-1)^s * 2^E * m

	   if the rounding of this quantity overflows

	 * rl is set to infinity, with the same sign
	   as the one of rh.

   In no case, a NaN is produced by this function.

   This function does not signal any exceptions.

*/
static inline __fp_status_t __binary64_round_to_pair_of_fp_numbers(__binary64_t * restrict rh,
								   __binary64_t * restrict rl,
								   int s, int32_t E, __uint128_t m) {
  unsigned int lzc, llzc;
  __uint128_t mm, mmr, nmmr, nmm, ml;
  int32_t EE, EH, ER = 0, EER, F, EL, EEL;
  uint64_t mh, tml, mml;
  __binary64_t resh, resl;
  int sr = 0, sl;
  uint64_t mr = 0ull;
  __fp_class_t resh_class;
  __fp_status_t statush, statusl;
  
  /* Normalize m, unless it is zero */
  if (m == ((__uint128_t) 0)) {
    mm = m;
    EE = E;
  } else {
    lzc = __lzc128(m);
    mm = m << lzc;
    EE = E - ((int32_t) lzc);
  }

  /* Make room for 3 bits of exponent change/carry.

     Use round to odd in order not to affect the 
     subsequent roundings to nearest.

  */
  mm = (mm >> 3) |
    ((__uint128_t) (!!((mm & ((__uint128_t) 7)) != ((__uint128_t) 0))));
  EE += (int32_t) 3;
  
  /* Get higher 64bit word out of m, using round to odd in 
     order to maintain the correctness of the subsequent 
     rounding to nearest.
  */
  mh = (uint64_t) (mm >> 64);
  tml = (uint64_t) mm;
  mh |= (uint64_t) (!!(tml != ((uint64_t) 0)));
  EH = EE + ((int32_t) 64);

  /* Round (-1)^s * 2^EH * mh to the nearest binary64 FP number,
     breaking ties to zero.
  */
  statush = (__fp_status_t) 0;
  resh = __binary64_round_to_nearest_ties_toward_zero(s, EH, mh, &statush);

  /* Convert the binary64 FP number resh back to a representation 

     (-1)^sr * 2^ER * mr

  */
  resh_class = __binary64_decompose(&sr, &ER, &mr, resh);

  /* Check if the rounding overflowed */
  if (resh_class == FP_CLASS_INFINITY) {
    /* Set resl to the same infinity */
    resl = resh;

    /* Write back the result and return overflow status */
    *rh = resh;
    *rl = resl;
    return (FP_STATUS_OVERFLOW | FP_STATUS_PRECISION);
  }

  /* Check if the rounding underflowed completely or
     if mh was zero (and hence mm was).
  */
  if (resh_class == FP_CLASS_ZERO) {
    /* Set resl to the same zero */
    resl = resh;

    /* Write back the result and return the appropriate status */
    *rh = resh;
    *rl = resl;

    /* If mh was zero, return empty status, as rounding 
       zero is exact. Otherwise return underflow and precision.
    */
    if (mh == ((uint64_t) 0)) return ((__fp_status_t) 0);
    return (FP_STATUS_UNDERFLOW | FP_STATUS_PRECISION);
  }

  /* Here resh is a finite, non-zero FP number, equal to 

     (-1)^sr * 2^ER * mr.

     Its sign sr is always equal to the original sign s.

     Convert mr to a 128 bit value, adjusting the exponent.

     We need to take into account that mr is normalized
     such that 

     2^(k - 1) <= mr < 2^k

     where k is the precision of binary64 FP numbers.

     The same way mm is normalized with 3 headroom bits (see above) 
     such that 

     2^(128 - 1 - 3) <= mm < 2^(128 - 3) - 1

  */
  mmr = ((__uint128_t) mr) << (64 + (64 - (BINARY64_MANTISSA_LENGTH + 1)));
  EER = ER - ((int32_t) (64 + (64 - (BINARY64_MANTISSA_LENGTH + 1))));

  /* Compute 

     (-1)^s * 2^EE * mm - (-1)^s * 2^EER * mmr
  
     The difference between the exponents EE and EER is bounded by 1,
     as 2^EER * mmr is the rounding to nearest of 2^EE * mm.

     As both 128 bit words are not used completely (there is a
     headroom of 3 bits), we can normalize both to the least of both
     exponents and obtain

     (-1)^s * 2^F * (nmm - nmmr);

     After normalization, either nmm is the greater or nmmr is.
     As both are unsigned, we need to handle the sign manually
     and possibly flip the sign s.

     Eventually we obtain 

     (-1)^sl * 2^EL * ml = (-1)^s * 2^F * (nmm - nmmr),

     where ml is normalized such that 

     2^(128 - 1) <= ml < 2^128,

     unless ml is zero, in which case sl is overridden 
     with the sign sr.

  */
  F = EE;
  if (EER < F) {
    F = EER;
  }
  nmm  = mm  << (unsigned int) (EE - F);
  nmmr = mmr << (unsigned int) (EER - F);
  EL = F;
  if (nmm >= nmmr) {
    sl = s;
    ml = nmm - nmmr;
  } else {
    sl = !s;
    ml = nmmr - nmm;
  }
  if (ml == ((__uint128_t) 0)) {
    sl = sr;
  } else {
    llzc = __lzc128(ml);
    ml <<= llzc;
    EL -= (int32_t) llzc;
  }

  /* Convert (truncate with rounding to odd) 

     (-1)^sl * 2^EL * ml 

     to 

     (-1)^sl * 2^EEL * mml

     where mml holds on a 64 bit variable.

  */
  mml = ((uint64_t) (ml >> 64)) |
    ((uint64_t) (!!(((uint64_t) ml) != ((uint64_t) 0))));
  EEL = EL + ((int32_t) 64);

  /* Round 

     (-1)^sl * 2^EEL * mml

     to the nearest binary64 FP number, breaking ties to zero.

     This yields resl.

     No overflow can happen on this rounding.

  */
  statusl = (__fp_status_t) 0;
  resl = __binary64_round_to_nearest_ties_toward_zero(sl, EEL, mml, &statusl);

  /* Write back the results */
  *rh = resh;
  *rl = resl;

  /* Return the appropriate status */
  return statusl;
}

static inline void __realize_fp_status(__fp_status_t status) {
  volatile __binary64_t a, b, c;

  /* If no status flag is set, there is nothing to do */
  if (status == ((__fp_status_t) 0)) return;

  /* Load two constants such that if they are multiplied they provoke
     the same IEEE754 flag setting as the one given by the status. 
  */
  if ((status & FP_STATUS_OVERFLOW) != ((__fp_status_t) 0)) {
    if ((status & FP_STATUS_PRECISION) != ((__fp_status_t) 0)) {
      a = 0x1.fffffffffffffp1023;
      b = 0x1.fffffffffffffp1023;
    } else {
      a = 0x1.0p1023;
      b = 0x1.0p1023;
    }
  } else {
    if ((status & FP_STATUS_UNDERFLOW) != ((__fp_status_t) 0)) {
      if ((status & FP_STATUS_PRECISION) != ((__fp_status_t) 0)) {
	a = 0x1.fffffffffffffp-600;
	b = 0x1.0p-450;
      } else {
	a = 0x1.95fcp-543;
	b = 0x1.0p-500;
      }
    } else {
      if ((status & FP_STATUS_PRECISION) != ((__fp_status_t) 0)) {
	a = 0x1.fffffffffffffp0;
	b = 0x1.fffffffffffffp0;
      } else {
	a = 1.0;
	b = 1.0;
      }
    }
  }

  /* Execute dummy operation to set IEEE754 flags */
  c = a * b;
  
  /* "Use" c */
  (void) c;
}

void BINARY64_AUGMENTED_ADD (__binary64_t *resh, __binary64_t *resl, __binary64_t a, __binary64_t b) {
  __fp_class_t ca, cb;
  int sa, sb, sl, sh, sr;
  int32_t EA, EB, EL, EH, ER;
  uint64_t ma, mb, ml, mh;
  __binary64_t rh, rl, ih, il;
  unsigned int sigma;
  __int128_t mmh, mml, acc;
  __uint128_t temp, mr;
  __fp_status_t status;

  /* Decompose a and b into 

     (-1)^sa * 2^EA * ma 

     and

     (-1)^sb * 2^EB * mb

     respectively the FP classes a and b fall into.

  */
  ca = __binary64_decompose(&sa, &EA, &ma, a);
  cb = __binary64_decompose(&sb, &EB, &mb, b);

  /* If one of the binary64 FP data a and b is not a non-zero finite
     number, we need to handle a special case.
  */
  if ((ca != FP_CLASS_NUMBER) || (cb != FP_CLASS_NUMBER)) {
    /* Special cases 

       If one of the input is a zero and the other is a finite, non-zero number, 
       
       * resh is the finite, non-zero number and
       * resl is zero with the sign of resh.

       In all other special cases, including the case when both inputs are zero,

       * resh = fl(a + b) where fl() indicates IEEE754 FP arithmetic
       * resl = resh.

       For infinities of like sign, this means infinity of the same
       sign as the input in both outputs. 

       For infinities of different signs, this means the same NaN in 
       output.

       For NaN inputs, this means the same NaN in output as one of the
       inputs.

    */
    if ((ca == FP_CLASS_NUMBER) && (cb == FP_CLASS_ZERO)) {
      rh = a;
      rl = 0.0; rl *= rl;
      if (sa) {
	rl = -rl;
      }
      *resh = rh;
      *resl = rl;
      return;
    }
    if ((ca == FP_CLASS_ZERO) && (cb == FP_CLASS_NUMBER)) {
      rh = b;
      rl = 0.0; rl *= rl;
      if (sb) {
	rl = -rl;
      }
      *resh = rh;
      *resl = rl;
      return;
    }
    
    /* Common special case

       Here at least one of the inputs is NaN or Inf, or both inputs 
       are zero.

    */
    
    rh = a + b; /* Signals invalid if required and quietizes NaNs */
    rl = rh;

    /* Write back the results */
    *resh = rh;
    *resl = rl;
    return;
  }

  /* Here, both a and b are finite, non-zero numbers, 
     decomposed into

     a = (-1)^sa * 2^EA * ma

     and

     b = (-1)^sb * 2^EB * mb.

     We start by ordering the numbers by 
     increasing exponents. 

  */
  if (EA <= EB) {
    EL = EA;
    ml = ma;
    sl = sa;
    il = a;
    EH = EB;
    mh = mb;
    sh = sb;
    ih = b;
  } else {
    EL = EB;
    ml = mb;
    sl = sb;
    il = b;
    EH = EA;
    mh = ma;
    sh = sa;
    ih = a;
  }

  /* If the difference between EH and EL is larger than the
     precision k = 53 of binary64 plus 2 bits, we are sure that 
     the the results resh is the input with the greater exponent and 
     resl is the input with the lesser exponent.

     Otherwise, we have an upper bound on the exponent difference.

  */
  if ((EH - EL) > ((int32_t) ((BINARY64_MANTISSA_LENGTH + 1) + 2))) {
    *resh = ih;
    *resl = il;
    return;
  }

  /* Here, we know that 

     EH - EL <= k + 2

     where k = 53 is the precision of the binary64 FP input numbers.

     We need to compute 

     (-1)^sh * 2^EH * mh + (-1)^sl * 2^EL * ml

     We rewrite this expression as follows:

        (-1)^sh * 2^EH * mh + (-1)^sl * 2^EL * ml = 

     =  (-1)^sh * 2^EL * (2^(EH - EL) * mh + (-1)^sl * (-1)^sh * ml)

     We start by expressing 

     2^(EH - EL) * mh 

     on a signed 128 bit variable and

     (-1)^sl * (-1)^sh * ml 

     on a signed 128 bit variable.

  */
  sigma = (unsigned int) (EH - EL);
  temp = (__uint128_t) mh;
  temp <<= sigma;
  mmh = (__int128_t) temp;
  temp = (__uint128_t) ml;
  mml = (__int128_t) temp;
  if (!!((!!sh) ^ (!!sl))) {
    mml = -mml;
  } 

  /* We now add mmh to mml and 
     then strip off its sign again.

     Eventually we will have 

     (-1)^sr * 2^ER * mr = (-1)^sh * 2^EH * mh + (-1)^sl * 2^EL * ml

  */
  sr = !!sh;
  ER = EL;
  acc = mmh + mml;
  if (acc < ((__int128_t) 0)) {
    acc = -acc;
    sr = !sr;
  }
  mr = (__uint128_t) acc;

  /* In the case when mr is zero, we need to produce a zero with 
     a positive sign.
  */
  if (mr == ((__uint128_t) 0)) {
    sr = 0;
  }

  /* Now we produce a binary64 FP expansion with 2 terms out of 

     (-1)^mr * 2^ER * mr

  */
  status = __binary64_round_to_pair_of_fp_numbers(&rh, &rl, sr, ER, mr);

  /* Realize the appropriate FP status */
  __realize_fp_status(status);
  
  /* Write back the result */
  *resh = rh;
  *resl = rl;
}

void BINARY64_AUGMENTED_MUL (__binary64_t *resh, __binary64_t *resl, __binary64_t a, __binary64_t b) {
  __fp_class_t ca, cb;
  int sa = 0, sb, sr;
  int32_t EA, EB, ER;
  uint64_t ma, mb;
  __binary64_t rh, rl;
  __uint128_t mma, mmb, mr;
  __fp_status_t status;

  /* Decompose a and b into 

     (-1)^sa * 2^EA * ma 

     and

     (-1)^sb * 2^EB * mb

     respectively the FP classes a and b fall into.

  */
  ca = __binary64_decompose(&sa, &EA, &ma, a);
  cb = __binary64_decompose(&sb, &EB, &mb, b);

  /* If one of the binary64 FP data a and b is not a non-zero finite
     number, we need to handle a special case.
  */
  if ((ca != FP_CLASS_NUMBER) || (cb != FP_CLASS_NUMBER)) {
    /* Special cases 

       In all special cases, including the case when one or both
       inputs are zero,

       * resh = fl(a + b) where fl() indicates IEEE754 FP arithmetic
       * resl = resh.

    */

    rh = a * b; /* Signals invalid if required and quietizes NaNs */
    rl = rh;

    /* Write back the results */
    *resh = rh;
    *resl = rl;
    return;
  }

  /* Here, both a and b are finite, non-zero numbers, 
     decomposed into

     a = (-1)^sa * 2^EA * ma

     and

     b = (-1)^sb * 2^EB * mb.

     We compute 

     a * b = (-1)^sa * 2^EA * ma * (-1)^sb * 2^EB * mb

     as 

     a * b = (-1)^sr * 2^ER * mr

     where 

     sr = sa XOR sb

     ER = EA + EB

     mr = ma * mb.

  */
  sr = !!((!!sa) ^ (!!sb));
  ER = EA + EB;
  mma = (__uint128_t) ma;
  mmb = (__uint128_t) mb;
  mr = mma * mmb;

  /* Now we produce a binary64 FP expansion with 2 terms out of 

     (-1)^mr * 2^ER * mr

  */
  status = __binary64_round_to_pair_of_fp_numbers(&rh, &rl, sr, ER, mr);

  /* Realize the appropriate FP status */
  __realize_fp_status(status);

  /* Write back the result */
  *resh = rh;
  *resl = rl;  
}

