/*
 * Copyright 2019-2020 by 
 * 
 * University of Alaska Anchorage
 * College of Engineering
 * Department of Computer Science
 * Anchorage, AK, USA
 *
 * Univ Lyon, CNRS, ENS de Lyon, Inria, Universite Claude Bernard Lyon
 * 1, LIP UMR 5668
 * Lyon, France
 *
 * and by
 * 
 * Universite Paris-Saclay, Univ Paris-Sud, CNRS, Inria, Laboratoire
 * de recherche en informatique,
 * Orsay, France.
 *
 * 
 * IEEE754-2019 Augmented Operations Reference Implementation
 *
 *
 * Contributor: Christoph Quirin Lauter 
 *              (University of Alaska Anchorage) 
 *              christoph.lauter@christoph-lauter.org
 *
 * This software implementation of the IEEE754-209 Augmented
 * Operations is based on the article
 * 
 * S. Boldo, Ch. Lauter, J.-M. Muller: Emulating Round-to-Nearest
 * Ties-to-Zero "Augmented" Floating-Point Operations Using
 * Round-to-Nearest Ties-to-Even Arithmetic
 * 
 * available at
 *
 * https://hal.archives-ouvertes.fr/hal-02137968v3
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

typedef double __binary64_t;

#define BINARY64_PSI                                                          ((0x1.fffffffffffffp-1))

static inline __binary64_t __binary64_fma(__binary64_t a, __binary64_t b, __binary64_t c) {
  return __builtin_fma(a, b, c);
}

static inline __binary64_t __binary64_fabs(__binary64_t a) {
  return __builtin_fabs(a);
}

static inline void __binary64_fast_twosum(__binary64_t * restrict rh, __binary64_t * restrict rl,
					  __binary64_t a, __binary64_t b) {
  __binary64_t resh, resl, t;

  resh = a + b;
  t = resh - a;
  resl = b - t;
  *rh = resh;
  *rl = resl;
}

static inline void __binary64_twomul(__binary64_t * restrict rh, __binary64_t * restrict rl,
				     __binary64_t a, __binary64_t b) {
  __binary64_t resh, resl, t;

  resh = a * b;
  t = -resh;
  resl = __binary64_fma(a, b, t);
  *rh = resh;
  *rl = resl;
}

static inline void __binary64_recomp(__binary64_t * restrict rh, __binary64_t * restrict rl,
				     __binary64_t a, __binary64_t b) {
  __binary64_t resh, resl, z, delta, t;

  z = BINARY64_PSI * a;
  delta = z - a;
  t = b + b;
  if (t == delta) {
    resh = z;
    resl = -b;
  } else {
    resh = a;
    resl = b;
  }
  *rh = resh;
  *rl = resl;
}

static inline void __binary64_aa_simple(__binary64_t * restrict rh, __binary64_t * restrict rl,
					__binary64_t a, __binary64_t b) {
  __binary64_t x, y, ae, be;

  if (__binary64_fabs(b) > __binary64_fabs(a)) {
    x = b;
    y = a;
  } else {
    x = a;
    y = b;
  }
  __binary64_fast_twosum(&ae, &be, x, y);
  __binary64_recomp(rh, rl, ae, be);
}

static inline void __binary64_am_simple(__binary64_t * restrict rh, __binary64_t * restrict rl,
					__binary64_t a, __binary64_t b) {
  __binary64_t ae, be;
  __binary64_twomul(&ae, &be, a, b);
  __binary64_recomp(rh, rl, ae, be);
}

void binary64_augmented_add(__binary64_t *rh, __binary64_t *rl, __binary64_t a, __binary64_t b) {
  __binary64_t th, tl;
  __binary64_aa_simple(&th, &tl, a, b);
  *rh = th;
  *rl = tl;
}

void binary64_augmented_mul(__binary64_t *rh, __binary64_t *rl, __binary64_t a, __binary64_t b) {
  __binary64_t th, tl;
  __binary64_am_simple(&th, &tl, a, b);
  *rh = th;
  *rl = tl;
}


