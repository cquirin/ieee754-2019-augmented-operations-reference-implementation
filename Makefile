# Copyright 2019-2020 by 
#
# University of Alaska Anchorage
# College of Engineering
# Department of Computer Science
# Anchorage, AK, USA
#
# Univ Lyon, CNRS, ENS de Lyon, Inria, Universite Claude Bernard Lyon
# 1, LIP UMR 5668
# Lyon, France
#
# and by
#
# Universite Paris-Saclay, Univ Paris-Sud, CNRS, Inria, Laboratoire
# de recherche en informatique,
# Orsay, France.
#
#
# IEEE754-2019 Augmented Operations Reference Implementation
#
#
# Contributor: Christoph Quirin Lauter 
#              (University of Alaska Anchorage) 
#              christoph.lauter@christoph-lauter.org
#
# This software implementation of the IEEE754-209 Augmented
# Operations is based on the article
#
# S. Boldo, Ch. Lauter, J.-M. Muller: Emulating Round-to-Nearest
# Ties-to-Zero "Augmented" Floating-Point Operations Using
# Round-to-Nearest Ties-to-Even Arithmetic
#
# available at
#
# https://hal.archives-ouvertes.fr/hal-02137968v3
#
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 3 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.


all: augmented-test-emul augmented-test-fp augmented-test-fp-simple augmented-perf-emul augmented-perf-fp augmented-perf-fp-simple

augmented-test-emul: augmented-test.o augmented-emul.o
	gcc -Wall -o $@ $^ -lm

augmented-test-fp: augmented-test.o augmented-fp.o
	gcc -Wall -o $@ $^ -lm

augmented-test-fp-simple: augmented-test.o augmented-fp-simple.o
	gcc -Wall -o $@ $^ -lm

augmented-perf-emul: augmented-perf.o augmented-empty.o augmented-emul.o augmented-reference.o
	gcc -Wall -o $@ $^ -lm

augmented-perf-fp: augmented-perf.o augmented-empty.o augmented-fp.o augmented-reference.o
	gcc -Wall -o $@ $^ -lm

augmented-perf-fp-simple: augmented-perf.o augmented-empty.o augmented-fp-simple.o augmented-reference.o
	gcc -Wall -o $@ $^ -lm

augmented-perf.o: augmented-perf.c
	gcc -Wall -O0 -march=native -c $<

augmented-empty.o: augmented-empty.c
	gcc -Wall -O3 -march=native -c $<

augmented-test.o: augmented-test.c
	gcc -Wall -O0 -march=native -c $<

augmented-emul.o: augmented-emul.c
	gcc -Wall -O3 -march=native -c $<

augmented-reference.o: augmented-emul.c
	gcc -Wall -O3 -march=native -DAUGMENTED_EMULATION_AS_REFERENCE=1 -c $< -o $@

augmented-fp.o: augmented-fp.c
	gcc -Wall -O3 -march=native -c $<

augmented-fp-simple.o: augmented-fp-simple.c
	gcc -Wall -O3 -march=native -c $<

check: test

test: test-emul test-fp test-fp-simple

test-emul: augmented-test-emul
	./augmented-test-emul augmented-add-test-vector.dat && echo "Okay add emul" || echo "Failure add emul"
	./augmented-test-emul augmented-mul-test-vector.dat && echo "Okay mul emul" || echo "Failure mul emul"

test-fp: augmented-test-fp
	./augmented-test-fp --ignoreFlags augmented-add-test-vector.dat && echo "Okay add fp" || echo "Failure add fp"
	./augmented-test-fp --ignoreFlags augmented-mul-test-vector.dat && echo "Okay mul fp" || echo "Failure mul fp"

test-fp-simple: augmented-test-fp-simple
	./augmented-test-fp-simple --ignoreFlags augmented-add-test-vector.dat && echo "Okay add fp simple" || echo "Failure add fp simple"
	./augmented-test-fp-simple --ignoreFlags augmented-mul-test-vector.dat && echo "Okay mul fp simple" || echo "Failure mul fp simple"

perf: augmented-perf-emul-add-all.eps augmented-perf-emul-mul-all.eps augmented-perf-fp-add-all.eps augmented-perf-fp-mul-all.eps augmented-perf-fp-simple-add-all.eps augmented-perf-fp-simple-mul-all.eps augmented-perf-emul-add-miulp.eps augmented-perf-emul-mul-miulp.eps augmented-perf-fp-add-miulp.eps augmented-perf-fp-mul-miulp.eps augmented-perf-fp-simple-add-miulp.eps augmented-perf-fp-simple-mul-miulp.eps timings.dat

timings.dat: augmented-perf-emul-add-all.dat augmented-perf-emul-mul-all.dat augmented-perf-fp-add-all.dat augmented-perf-fp-mul-all.dat augmented-perf-fp-simple-add-all.dat augmented-perf-fp-simple-mul-all.dat augmented-perf-emul-add-miulp.dat augmented-perf-emul-mul-miulp.dat augmented-perf-fp-add-miulp.dat augmented-perf-fp-mul-miulp.dat augmented-perf-fp-simple-add-miulp.dat augmented-perf-fp-simple-mul-miulp.dat
	echo "Average timings in cycles: " > $@
	for f in $^; do echo -n "$$f" | sed -e 's/^augmented-perf-//g;s/\.dat$$/ : /g;' >> $@; cat "$$f" | grep "^# avg:" | sed -e 's/^# avg: *//g;s/ *$$//g;' >> $@; done

%.p: %.dat
	echo "set terminal postscript eps color" > $@
	echo "set out \""$(@:.p=.eps)"\"" >> $@
	echo "set format x \"%g\"" >> $@
	echo "set format y \"%g\"" >> $@
	echo "plot \""$<"\" using 1:3 with boxes fs solid 1.0 t \"\"" >> $@

%.eps: %.p 
	gnuplot $<

augmented-perf-emul-add-all.dat: augmented-perf-emul
	./augmented-perf-emul $@ add all 1000000 100 200 99 100 1

augmented-perf-emul-mul-all.dat: augmented-perf-emul
	./augmented-perf-emul $@ mul all 1000000 100 200 99 300 1

augmented-perf-fp-add-all.dat: augmented-perf-fp
	./augmented-perf-fp $@ add all 1000000 100 200 99 100 1

augmented-perf-fp-mul-all.dat: augmented-perf-fp
	./augmented-perf-fp $@ mul all 1000000 100 200 99 300 1

augmented-perf-fp-simple-add-all.dat: augmented-perf-fp-simple
	./augmented-perf-fp-simple $@ add correct 1000000 100 200 99 100 1

augmented-perf-fp-simple-mul-all.dat: augmented-perf-fp-simple
	./augmented-perf-fp-simple $@ mul correct 1000000 100 200 99 300 1


augmented-perf-emul-add-miulp.dat: augmented-perf-emul
	./augmented-perf-emul $@ add miulp 1000000 100 200 99 100 1

augmented-perf-emul-mul-miulp.dat: augmented-perf-emul
	./augmented-perf-emul $@ mul miulp 1000000 100 200 99 300 1

augmented-perf-fp-add-miulp.dat: augmented-perf-fp
	./augmented-perf-fp $@ add miulp 1000000 100 200 99 100 1

augmented-perf-fp-mul-miulp.dat: augmented-perf-fp
	./augmented-perf-fp $@ mul miulp 1000000 100 200 99 300 1

augmented-perf-fp-simple-add-miulp.dat: augmented-perf-fp-simple
	./augmented-perf-fp-simple $@ add miulpcorrect 1000000 100 200 99 100 1

augmented-perf-fp-simple-mul-miulp.dat: augmented-perf-fp-simple
	./augmented-perf-fp-simple $@ mul miulpcorrect 1000000 100 200 99 300 1



clean:
	rm -f augmented-emul.o augmented-reference.o augmented-fp.o augmented-fp-simple.o augmented-test.o augmented-test-emul augmented-test-fp augmented-test-fp-simple augmented-perf-emul augmented-perf-fp augmented-perf-fp-simple augmented-perf.o augmented-empty.o augmented-perf-fp-add-all.dat augmented-perf-fp-mul-all.dat augmented-perf-fp-simple-add-all.dat augmented-perf-fp-simple-mul-all.dat augmented-perf-emul-add-all.dat augmented-perf-emul-mul-all.dat augmented-perf-fp-add-all.eps augmented-perf-fp-mul-all.eps augmented-perf-fp-simple-add-all.eps augmented-perf-fp-simple-mul-all.eps augmented-perf-emul-add-all.eps augmented-perf-emul-mul-all.eps augmented-perf-fp-add-all.p augmented-perf-fp-mul-all.p augmented-perf-fp-simple-add-all.p augmented-perf-fp-simple-mul-all.p augmented-perf-emul-add-all.p augmented-perf-emul-mul-all.p augmented-perf-fp-add-miulp.dat augmented-perf-fp-mul-miulp.dat augmented-perf-fp-simple-add-miulp.dat augmented-perf-fp-simple-mul-miulp.dat augmented-perf-emul-add-miulp.dat augmented-perf-emul-mul-miulp.dat augmented-perf-fp-add-miulp.eps augmented-perf-fp-mul-miulp.eps augmented-perf-fp-simple-add-miulp.eps augmented-perf-fp-simple-mul-miulp.eps augmented-perf-emul-add-miulp.eps augmented-perf-emul-mul-miulp.eps augmented-perf-fp-add-miulp.p augmented-perf-fp-mul-miulp.p augmented-perf-fp-simple-add-miulp.p augmented-perf-fp-simple-mul-miulp.p augmented-perf-emul-add-miulp.p augmented-perf-emul-mul-miulp.p timings.dat


.PHONY: all clean test test-emul test-fp check perf

