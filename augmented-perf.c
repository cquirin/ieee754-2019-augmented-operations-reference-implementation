/*
 * Copyright 2019-2020 by 
 * 
 * University of Alaska Anchorage
 * College of Engineering
 * Department of Computer Science
 * Anchorage, AK, USA
 *
 * Univ Lyon, CNRS, ENS de Lyon, Inria, Universite Claude Bernard Lyon
 * 1, LIP UMR 5668
 * Lyon, France
 *
 * and by
 * 
 * Universite Paris-Saclay, Univ Paris-Sud, CNRS, Inria, Laboratoire
 * de recherche en informatique,
 * Orsay, France.
 *
 * 
 * IEEE754-2019 Augmented Operations Reference Implementation
 *
 *
 * Contributor: Christoph Quirin Lauter 
 *              (University of Alaska Anchorage) 
 *              christoph.lauter@christoph-lauter.org
 *
 * This software implementation of the IEEE754-209 Augmented
 * Operations is based on the article
 * 
 * S. Boldo, Ch. Lauter, J.-M. Muller: Emulating Round-to-Nearest
 * Ties-to-Zero "Augmented" Floating-Point Operations Using
 * Round-to-Nearest Ties-to-Even Arithmetic
 * 
 * available at
 *
 * https://hal.archives-ouvertes.fr/hal-02137968v3
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fenv.h>
#include <time.h>

#define READ_TIME_COUNTER(time)                              \
        __asm__ __volatile__(                                \
                "xorl %%eax,%%eax\n\t"                       \
                "cpuid\n\t"                                  \
                "rdtsc\n\t"                                  \
                "movl %%eax,(%0)\n\t"                        \
                "movl %%edx,4(%0)\n\t"                       \
                "xorl %%eax,%%eax\n\t"                       \
                "cpuid\n\t"                                  \
                : /* nothing */                              \
		: "S"((time))				     \
                : "eax", "ebx", "ecx", "edx", "memory")

typedef double __binary64_t;

typedef union {
  __binary64_t f;
  uint64_t     i;
} __binary64_conv_t;

typedef enum {
  FP_CLASS_NORMAL,
  FP_CLASS_SUBNORMAL,
  FP_CLASS_ZERO,
  FP_CLASS_INF,
  FP_CLASS_NAN
} __fp_class_t;

enum __operation_enum_t {
  AUGMENTED_ADD,
  AUGMENTED_MUL
};
typedef enum __operation_enum_t __operation_t;

enum __input_enum_type_t {
  ALL_INPUTS,
  MIULP_INPUTS,
  ALL_CORRECT_INPUTS,
  MIULP_CORRECT_INPUTS
};
typedef enum __input_enum_type_t __input_type_t;


typedef struct {
  uint64_t samples;
  uint64_t max_val;
  uint64_t bucket_size;
  size_t data_len;
  uint64_t sum_hi;
  uint64_t sum_lo;
  uint64_t * data;
} __histogram_t;

void binary64_augmented_add(__binary64_t *, __binary64_t *, __binary64_t, __binary64_t);
void binary64_augmented_mul(__binary64_t *, __binary64_t *, __binary64_t, __binary64_t);

void binary64_augmented_empty(__binary64_t *, __binary64_t *, __binary64_t, __binary64_t);

void __ref_binary64_augmented_add(__binary64_t *, __binary64_t *, __binary64_t, __binary64_t);
void __ref_binary64_augmented_mul(__binary64_t *, __binary64_t *, __binary64_t, __binary64_t);


__fp_class_t binary64_classify(__binary64_t x) {
  __binary64_conv_t t;

  t.f = x;
  if ((t.i & ((uint64_t) 0x7ff0000000000000ull)) == ((uint64_t) 0x0)) {
    /* Zero or subnormal */
    if ((t.i & ((uint64_t) 0x000fffffffffffffull)) == ((uint64_t) 0x0)) {
      /* Zero */
      return FP_CLASS_ZERO;
    }
    /* Subnormal */
    return FP_CLASS_SUBNORMAL;
  }
  if ((t.i & ((uint64_t) 0x7ff0000000000000ull)) == ((uint64_t) 0x7ff0000000000000ull)) {
    /* Inf or NaN */
    if ((t.i & ((uint64_t) 0x000fffffffffffffull)) == ((uint64_t) 0x0)) {
      /* Inf */
      return FP_CLASS_INF;
    }
    /* NaN */
    return FP_CLASS_NAN;    
  }
  /* Normal */
  return FP_CLASS_NORMAL;
}

uint16_t random_uint16() {
  long int r;

  r = lrand48();
  
  return ((uint16_t) r);
}

uint64_t random_uint64() {
  uint64_t t;

  t = (uint64_t) random_uint16();
  t <<= 16;
  t |= (uint64_t) random_uint16();
  t <<= 16;
  t |= (uint64_t) random_uint16();
  t <<= 16;
  t |= (uint64_t) random_uint16();
  return t;
}

int random_boolean() {
  uint16_t r;

  r = random_uint16();

  return (!!((int) (r & ((uint16_t) 1))));
}

__binary64_t __random_binary64_normal() {
  __binary64_conv_t t;

  do {
    t.i = random_uint64();
  } while (binary64_classify(t.f) != FP_CLASS_NORMAL);
  return t.f;
}

__binary64_t __random_binary64_subnormal() {
  __binary64_conv_t t;

  t.i = random_uint64();
  t.i &= (uint64_t) 0x800fffffffffffffull;
  return t.f;
}

__binary64_t __random_binary64_zero() {
  __binary64_conv_t t;

  t.i = (uint64_t) (random_uint16() & ((uint16_t) 1));
  t.i <<= 63;
  return t.f;
}

__binary64_t __random_binary64_inf() {
  __binary64_conv_t t;

  t.i = (uint64_t) (random_uint16() & ((uint16_t) 1));
  t.i <<= 63;
  t.i |= (uint64_t) 0x7ff0000000000000ull;
  return t.f;
}

__binary64_t __random_binary64_nan() {
  __binary64_conv_t t;

  t.i = random_uint64();
  t.i &= (uint64_t) 0x8007ffffffffffffull;
  t.i |= (uint64_t) 0x7ff8000000000000ull;
  return t.f;
}

__binary64_t random_binary64(__fp_class_t fp_class) {
  switch (fp_class) {
  case FP_CLASS_NORMAL:
    return __random_binary64_normal();
    break;
  case FP_CLASS_SUBNORMAL:
    return __random_binary64_subnormal();
    break;
  case FP_CLASS_ZERO:
    return __random_binary64_zero();
    break;
  case FP_CLASS_INF:
    return __random_binary64_inf();
    break;  
  case FP_CLASS_NAN:
    return __random_binary64_nan();
    break;
  default:
    return 0.0;
    break;
  }
  return 0.0;
}

int binary64_is_finite(__binary64_t x) {
  switch (binary64_classify(x)) {
  case FP_CLASS_NORMAL:
  case FP_CLASS_SUBNORMAL:
  case FP_CLASS_ZERO:
    return 1;
    break;
  case FP_CLASS_INF:
  case FP_CLASS_NAN:
    return 0;
    break;
  default:
    return 0;
    break;
  }
  return 0;
}

void random_normal_inputs_with_finite_output_binary64(__binary64_t * res_x, __binary64_t * res_y, __operation_t op) {
  __binary64_t x, y, a, b;

  do {
    x = random_binary64(FP_CLASS_NORMAL);
    y = random_binary64(FP_CLASS_NORMAL);
    switch (op) {
    case AUGMENTED_ADD:
      __ref_binary64_augmented_add(&a, &b, x, y);      
      break;
    case AUGMENTED_MUL:
      __ref_binary64_augmented_mul(&a, &b, x, y);
      break;
    }
  } while (!(binary64_is_finite(a) && binary64_is_finite(b)));
  *res_x = x;
  *res_y = y;
}

void random_normal_add_inputs_with_miulp_finite_output_binary64(__binary64_t * res_x, __binary64_t * res_y) {
  __binary64_t x, y, sc;
  int16_t E;
  uint64_t k, t, m, n, c, b;
  int i, s;
  __binary64_conv_t scd;

  E = ((int16_t) (random_uint16() & ((((uint16_t) 1) << 10) - ((uint16_t) 1)))) - ((int16_t) (((uint16_t) 1) << 9));
  scd.i = ((uint64_t) (((int) E) + 1023)) << 52;
  sc = scd.f;
  k = (random_uint64() & ((((uint64_t) 1) << 52) - ((uint64_t) 1))) | (((uint64_t) 1) << 52);
  t = (k << 1) + ((uint64_t) 1);
  m = (uint64_t) 0;
  n = (uint64_t) 0;
  c = (uint64_t) 1;
  n = t & c;
  do {
    s = (int) (random_uint16() & ((((uint16_t) 1) << 6) - ((uint16_t) 1)));
  } while (!((0 <= s) && (s <= 52)));
  for (i=1;i<=s;i++) {
    c <<= 1;
    b = t & c;
    if (random_boolean()) {
      m |= b;
    } else {
      n |= b;
    }
  }
  for (;i<=53;i++) {
    c <<= 1;
    b = t & c;
    m |= b;
  }
  x = sc * m;
  y = sc * n;
  if (random_boolean()) {
    x = -x;
    y = -y;
  }
  *res_x = x;
  *res_y = y;
}

void random_normal_mul_inputs_with_miulp_finite_output_binary64(__binary64_t * res_x, __binary64_t * res_y) {
  uint64_t mp, np, km1, m, n;
  int16_t E, F;
  __binary64_t x, y;
  __binary64_conv_t sx, sy;

  do {
    mp = (random_uint64() & ((((uint64_t) 1) << 27) - ((uint64_t) 1))) + (((uint64_t) 1) << 24);
    np = (random_uint64() & ((((uint64_t) 1) << 27) - ((uint64_t) 1))) + (((uint64_t) 1) << 24);
    km1 = ((mp * np) << 1) - mp - np;
  } while (!(((((uint64_t) 1) << 52) <= km1) && (km1 < (((uint64_t) 1) << 53))));
  m = (mp << 1) - ((uint64_t) 1);
  n = (np << 1) - ((uint64_t) 1);
  x = (__binary64_t) m;
  y = (__binary64_t) n;
  E = ((int16_t) (random_uint16() & ((((uint16_t) 1) << 10) - ((uint16_t) 1)))) - ((int16_t) (((uint16_t) 1) << 9));
  F = ((int16_t) (random_uint16() & ((((uint16_t) 1) << 10) - ((uint16_t) 1)))) - ((int16_t) (((uint16_t) 1) << 9));
  sx.i = ((uint64_t) (((int) E) + 1023)) << 52;
  sy.i = ((uint64_t) (((int) F) + 1023)) << 52;
  if (random_boolean()) {
    sx.f = -sx.f;
  }
  if (random_boolean()) {
    sy.f = -sy.f;
  }
  *res_x = x * sx.f;
  *res_y = y * sy.f;  
}

void random_normal_inputs_with_miulp_finite_output_binary64(__binary64_t * res_x, __binary64_t * res_y, __operation_t op) {
  __binary64_t x, y, a, b, t;
  
  do {
    switch (op) {
    case AUGMENTED_ADD:
      random_normal_add_inputs_with_miulp_finite_output_binary64(&x, &y);
      if (random_boolean()) {
	t = x;
	x = y;
	y = t;
      }
      __ref_binary64_augmented_add(&a, &b, x, y);      
      break;
    case AUGMENTED_MUL:
      random_normal_mul_inputs_with_miulp_finite_output_binary64(&x, &y);
      if (random_boolean()) {
	t = x;
	x = y;
	y = t;
      }
      __ref_binary64_augmented_mul(&a, &b, x, y);
      break;
    }
  } while (!(binary64_is_finite(a) && binary64_is_finite(b)));
  *res_x = x;
  *res_y = y;  
}

int compare_binary64(__binary64_t a, __binary64_t b) {
  __binary64_conv_t ac, bc;

  ac.f = a;
  bc.f = b;
  
  return (ac.i == bc.i);
}

void random_normal_inputs_with_finite_correct_output_binary64(__binary64_t * res_x, __binary64_t * res_y, __operation_t op) {
  int okay;
  __binary64_t x, y, a_ref, b_ref, a_cmp, b_cmp;

  do {
    random_normal_inputs_with_finite_output_binary64(&x, &y, op);
    switch (op) {
    case AUGMENTED_ADD:
      __ref_binary64_augmented_add(&a_ref, &b_ref, x, y);
      binary64_augmented_add(&a_cmp, &b_cmp, x, y);
      break;
    case AUGMENTED_MUL:
      __ref_binary64_augmented_mul(&a_ref, &b_ref, x, y);
      binary64_augmented_mul(&a_cmp, &b_cmp, x, y);
      break;
    }
    okay = compare_binary64(a_ref, a_cmp) && compare_binary64(b_ref, b_cmp);
  } while (!okay);

  *res_x = x;
  *res_y = y;
}

void random_normal_inputs_with_miulp_finite_correct_output_binary64(__binary64_t * res_x, __binary64_t * res_y, __operation_t op) {
  int okay;
  __binary64_t x, y, a_ref, b_ref, a_cmp, b_cmp;

  do {
    random_normal_inputs_with_miulp_finite_output_binary64(&x, &y, op);
    switch (op) {
    case AUGMENTED_ADD:
      __ref_binary64_augmented_add(&a_ref, &b_ref, x, y);
      binary64_augmented_add(&a_cmp, &b_cmp, x, y);
      break;
    case AUGMENTED_MUL:
      __ref_binary64_augmented_mul(&a_ref, &b_ref, x, y);
      binary64_augmented_mul(&a_cmp, &b_cmp, x, y);
      break;
    }
    okay = compare_binary64(a_ref, a_cmp) && compare_binary64(b_ref, b_cmp);
  } while (!okay);

  *res_x = x;
  *res_y = y;
}

void generate_input_binary64(__binary64_t * res_x, __binary64_t * res_y, __operation_t op, __input_type_t input_type) {
  __binary64_t x, y;

  switch (input_type) {
  case ALL_INPUTS:
    random_normal_inputs_with_finite_output_binary64(&x, &y, op);
    break;
  case ALL_CORRECT_INPUTS:
    random_normal_inputs_with_finite_correct_output_binary64(&x, &y, op);
    break;    
  case MIULP_INPUTS:
    random_normal_inputs_with_miulp_finite_output_binary64(&x, &y, op);
    break;
  case MIULP_CORRECT_INPUTS:
    random_normal_inputs_with_miulp_finite_correct_output_binary64(&x, &y, op);
    break;    
  }
  *res_x = x;
  *res_y = y;
}

void histogram_add(__histogram_t *hist, uint64_t val) {
  uint64_t bucket;
  size_t idx;
  uint64_t *temp;
  
  if (hist->bucket_size < ((uint64_t) 1)) {
    hist->bucket_size = 1;
  }
  bucket = (uint64_t) ((((double) val) / ((double) hist->bucket_size)) + 0.5);
  idx = (size_t) bucket;
  if (((uint64_t) idx) != bucket) return;
  if (hist->data == NULL) {
    hist->data = (uint64_t *) calloc(idx + 1, sizeof(uint64_t));
    if (hist->data == NULL) return;
    hist->data_len = idx + 1;
  }
  if (idx > hist->data_len - 1) {
    temp = (uint64_t *) realloc(hist->data, (idx + 1) * sizeof(uint64_t));
    if (temp == NULL) return;
    hist->data = temp;
    memset(&hist->data[hist->data_len], 0, (idx + 1 - hist->data_len) * sizeof(uint64_t)); 
    hist->data_len = idx + 1;
  }
  hist->data[idx]++;
  hist->samples++;
  if (hist->max_val < val) {
    hist->max_val = val;
  }
  hist->sum_lo += val;
  if (hist->sum_lo < val) {
    hist->sum_hi++;
  }
}

void histogram_clear(__histogram_t *hist) {
  if (hist->data != NULL) {
    free(hist->data);
    hist->data_len = (size_t) 0;
    hist->data = NULL;
  }
}

void histogram_init(__histogram_t *hist) {
  hist->samples = (uint64_t) 0;
  hist->max_val = (uint64_t) 0;
  hist->bucket_size = (uint64_t) 1;
  hist->data_len = (size_t) 0;
  hist->data = NULL;
  hist->sum_hi = (uint64_t) 0;
  hist->sum_lo = (uint64_t) 0;
}

void histogram_copy(__histogram_t *out, __histogram_t *in) {
  histogram_clear(out);
  out->samples = in->samples;
  out->max_val = in->max_val;
  out->bucket_size = in->bucket_size;
  out->sum_hi = in->sum_hi;
  out->sum_lo = in->sum_lo;
  out->data = NULL;
  out->data_len = (size_t) 0;
  if (in->data != NULL) {
    out->data = (uint64_t *) calloc(in->data_len, sizeof(uint64_t));
    if (out->data == NULL) {
      histogram_init(out);
      return;
    }
    out->data_len = in->data_len;
    memcpy(out->data, in->data, in->data_len * sizeof(uint64_t));
  }
}

void histogram_compress(__histogram_t *out, __histogram_t *in, uint64_t bucket_size) {
  uint64_t bs, bucket;
  size_t idx;
  uint64_t *temp;
  uint64_t i, smp;

  bs = bucket_size;
  if (bs < ((uint64_t) 1)) {
    bs = (uint64_t) 1;
  }
  if (bs < in->bucket_size) {
    bs = in->bucket_size;
  }
  if (bs == in->bucket_size) {
    histogram_copy(out, in);
    return;
  }
  histogram_clear(out);
  out->samples = in->samples;
  out->max_val = in->max_val;
  out->sum_hi = in->sum_hi;
  out->sum_lo = in->sum_lo;
  out->bucket_size = bs;
  bucket = (uint64_t) ((((double) out->max_val) / ((double) out->bucket_size)) + 0.5);
  idx = (size_t) bucket;
  if (((uint64_t) idx) != bucket) {
    histogram_init(out);
    return;
  }
  out->data = (uint64_t *) calloc(idx + 1, sizeof(uint64_t));
  if (out->data == NULL) {
    histogram_init(out);
    return;
  }
  out->data_len = idx + 1;
  temp = (uint64_t *) calloc(in->data_len, sizeof(uint64_t));
  if (temp == NULL) {
    histogram_init(out);
    return;
  }
  memcpy(temp, in->data, in->data_len * sizeof(uint64_t));
  for (i=(uint64_t)0; i<=in->max_val; i++) {
    bucket = (uint64_t) ((((double) i) / ((double) in->bucket_size)) + 0.5);
    idx = (size_t) bucket;
    if (((uint64_t) idx) != bucket) {
      histogram_init(out);
      free(temp);
      return;
    }
    smp = temp[idx];
    temp[idx] = (uint64_t) 0;
    if (smp != ((uint64_t) 0)) {
      bucket = (uint64_t) ((((double) i) / ((double) out->bucket_size)) + 0.5);
      idx = (size_t) bucket;
      if (((uint64_t) idx) != bucket) {
	histogram_init(out);
	free(temp);
	return;
      }
      out->data[idx] += smp;
    }
  }
  free(temp);
}

void histogram_compress_resolution(__histogram_t *out, __histogram_t *in, uint64_t raw_buckets) {
  uint64_t buckets;
  buckets = raw_buckets;
  if (buckets < ((uint64_t) 1)) {
    buckets = (uint64_t) 1;
  }
  histogram_compress(out, in, (uint64_t) ((((double) in->max_val) / ((double) buckets)) + 0.5));
}

double histogram_get_average(__histogram_t *hist) {
  uint32_t c[4];
  int i;
  double t, scale, two32, v, avg;

  if (hist->samples == ((uint64_t) 0)) return 0.0;
  
  c[0] = (uint32_t) (hist->sum_lo);
  c[1] = (uint32_t) (hist->sum_lo >> 32);
  c[2] = (uint32_t) (hist->sum_hi);
  c[3] = (uint32_t) (hist->sum_hi >> 32);

  v = 0.0;
  scale = 1.0;
  two32 = 4294967296.0;
  for (i=0; i<4; i++) {
    t = (double) c[i];
    v += t * scale;
    scale *= two32;
  }
  avg = v / ((double) (hist->samples));
  return avg;
}

void histogram_print(FILE *fd, __histogram_t *hist, double percentage, uint64_t minBuckets) {
  size_t i;
  uint64_t val, accum;
  double p;

  if (hist->data == NULL) return;
  if (hist->bucket_size == ((uint64_t) 0)) return;
  if (hist->samples == ((uint64_t) 0)) return;
  fprintf(fd, "# avg: %7.2f\n", histogram_get_average(hist)); 
  accum = (uint64_t) 0;
  for (i=0; i<hist->data_len; i++) {
    val = (uint64_t) ((((double) i) + 0.5) * ((double) hist->bucket_size));
    fprintf(fd, "%8llu\t%24llu\t%7.2f\n",
	    ((unsigned long long int) val),
	    ((unsigned long long int) hist->data[i]),
	    ((((double) hist->data[i]) / ((double) hist->samples)) * 100.0));
    accum += hist->data[i];
    p = (((double) accum) / ((double) hist->samples)) * 100.0;
    if ((p > percentage) && (i >= minBuckets))
      break;
  }
  for (; i<minBuckets; i++) {
    val = (uint64_t) ((((double) i) + 0.5) * ((double) hist->bucket_size));
    fprintf(fd, "%8llu\t%24llu\t%7.2f\n",
	    ((unsigned long long int) val),
	    ((unsigned long long int) 0u),
	    0.0);
  }
}
    
void run_timing(__histogram_t *hist, __operation_t op, __input_type_t input_type, uint64_t raw_samples, uint64_t raw_runs) {
  unsigned long long int before, after;
  uint64_t samples, runs, i, j, time, total_time, max_time, empty_time;
  double avg_time;
  __binary64_t x, y, a, b;
  volatile __binary64_t sink;

  samples = raw_samples;
  if (samples < ((uint64_t) 1)) {
    samples = (uint64_t) 1;
  }
  runs = raw_runs;
  if (runs < ((uint64_t) 1)) {
    runs = (uint64_t) 1;
  }
  histogram_clear(hist);
  histogram_init(hist);
  sink = 0.0;
  for (i=0; i<samples; i++) {
    generate_input_binary64(&x, &y, op, input_type);
    total_time = (uint64_t) 0;
    for (j=0; j<runs; j++) {
      do {
	switch (op) {
	case AUGMENTED_ADD:
	  READ_TIME_COUNTER(&before);
	  binary64_augmented_empty(&a, &b, x, y);
	  READ_TIME_COUNTER(&after);
	  sink += a * b;
	  break;
	case AUGMENTED_MUL:
	  READ_TIME_COUNTER(&before);
	  binary64_augmented_empty(&a, &b, x, y);
	  READ_TIME_COUNTER(&after);
	  sink += a * b;
	  break;
	}
      } while (after <= before);
      time = (uint64_t) (after - before);
      total_time += time;
    }
    avg_time = ((double) total_time) / ((double) runs);
    max_time = (uint64_t) ((1.25 * avg_time) + 0.5);
    total_time = (uint64_t) 0;
    for (j=0; j<runs; j++) {
      do {
	do {
	  switch (op) {
	  case AUGMENTED_ADD:
	    READ_TIME_COUNTER(&before);
	    binary64_augmented_empty(&a, &b, x, y);
	    READ_TIME_COUNTER(&after);
	    sink += a * b;
	    break;
	  case AUGMENTED_MUL:
	    READ_TIME_COUNTER(&before);
	    binary64_augmented_empty(&a, &b, x, y);
	    READ_TIME_COUNTER(&after);
	    sink += a * b;
	    break;
	  }
	} while (after <= before);
	time = (uint64_t) (after - before);
      } while (time > max_time);
      total_time += time;
    }
    avg_time = ((double) total_time) / ((double) runs);
    empty_time = (uint64_t) (avg_time + 0.5);
    total_time = (uint64_t) 0;
    for (j=0; j<runs; j++) {
      do {
	do {
	  switch (op) {
	  case AUGMENTED_ADD:
	    READ_TIME_COUNTER(&before);
	    binary64_augmented_add(&a, &b, x, y);
	    READ_TIME_COUNTER(&after);
	    sink += a * b;
	    break;
	  case AUGMENTED_MUL:
	    READ_TIME_COUNTER(&before);
	    binary64_augmented_mul(&a, &b, x, y);
	    READ_TIME_COUNTER(&after);
	    sink += a * b;
	    break;
	  }
	} while (after <= before);
	time = (uint64_t) (after - before);
      } while (time <= empty_time);
      total_time += (time - empty_time);
    }
    avg_time = ((double) total_time) / ((double) runs);
    max_time = (uint64_t) ((1.25 * avg_time) + 0.5);
    total_time = (uint64_t) 0;
    for (j=0; j<runs; j++) {
      do {
	do {
	  do {
	    switch (op) {
	    case AUGMENTED_ADD:
	      READ_TIME_COUNTER(&before);
	      binary64_augmented_add(&a, &b, x, y);
	      READ_TIME_COUNTER(&after);
	      sink += a * b;
	      break;
	    case AUGMENTED_MUL:
	      READ_TIME_COUNTER(&before);
	      binary64_augmented_mul(&a, &b, x, y);
	      READ_TIME_COUNTER(&after);
	      sink += a * b;
	      break;
	    }
	  } while (after <= before);
	  time = (uint64_t) (after - before);
	} while (time <= empty_time);
	time -= empty_time;
      } while (time > max_time);
      total_time += time;
    }
    avg_time = ((double) total_time) / ((double) runs);
    histogram_add(hist, ((uint64_t) (avg_time + 0.5)));
  }
}

int main(int argc, char **argv) {
  __operation_t op;
  __input_type_t inputType;
  uint64_t samples, runs, resolution, minBuckets, fixedBucketSize;
  __histogram_t raw, out;
  double percentage;
  char *filename;
  FILE *fd;

  srand48((long int) time(NULL));
  
  histogram_init(&raw);
  histogram_init(&out);
  fixedBucketSize = (uint64_t) 0;
  if (argc < 9) {
    fprintf(stderr, "Not enough arguments.\n");
    return 1;
  }
  filename = argv[1];
  if (strcmp(argv[2], "add") == 0) {
    op = AUGMENTED_ADD;
  } else {
    if (strcmp(argv[2], "mul") == 0) {
      op = AUGMENTED_MUL;
    } else {
      fprintf(stderr, "Unsupported operation \"%s\".\n", argv[2]);
      return 1;
    }
  }
  if (strcmp(argv[3], "all") == 0) {
    inputType = ALL_INPUTS;
  } else {
    if (strcmp(argv[3], "correct") == 0) {
      inputType = ALL_CORRECT_INPUTS;
    } else {
      if (strcmp(argv[3], "miulp") == 0) {
	inputType = MIULP_INPUTS;
      } else {
        if (strcmp(argv[3], "miulpcorrect") == 0) {
	  inputType = MIULP_CORRECT_INPUTS;
	} else {
	  fprintf(stderr, "Unsupported input type \"%s\".\n", argv[3]);
	  return 1;
	}
      }
    }
  }
  samples = (uint64_t) atoll(argv[4]);
  runs = (uint64_t) atoll(argv[5]);
  resolution = (uint64_t) atoll(argv[6]);
  percentage = strtod(argv[7], NULL);
  if (percentage < 0.0) {
    percentage = 0.0;
  }
  if (percentage > 100.0) {
    percentage = 100.0;
  }
  minBuckets = (uint64_t) atoll(argv[8]);
  if (argc >= 10) {
    fixedBucketSize = (uint64_t) atoll(argv[9]);
    if (fixedBucketSize < ((uint64_t) 1)) {
      fixedBucketSize = (uint64_t) 1;
    }
  }

  fd = fopen(filename, "w");
  if (fd == NULL) {
    fprintf(stderr, "Could not open file \"%s\": %s.\n", filename, strerror(errno));
  }
  
  printf("Running timing: %s %s %llu %llu %llu %7.2f %llu", argv[2], argv[3],
	 ((unsigned long long int) samples),
	 ((unsigned long long int) runs),
	 ((unsigned long long int) resolution),
	 percentage,
	 ((unsigned long long int) minBuckets));
  if (fixedBucketSize != ((uint64_t) 0)) {
    printf(" %llu", ((unsigned long long int) fixedBucketSize));
  }
  printf("\n");

  run_timing(&raw, op, inputType, samples, runs);

  printf("Finished running the timing\n");

  if (fixedBucketSize == ((uint64_t) 0)) {
    histogram_compress_resolution(&out, &raw, resolution);
  } else {
    histogram_compress(&out, &raw, fixedBucketSize);
  }

  printf("Writing histogram data to file \"%s\"\n", filename);
  histogram_print(fd, &out, percentage, minBuckets);

  printf("The average timing is: %7.2f cycles.\n", histogram_get_average(&out));
  
  if (fclose(fd) != 0) {
    fprintf(stderr, "Could not close file \"%s\": %s.\n", filename, strerror(errno));
  }
  
  histogram_clear(&raw);
  histogram_clear(&out);
  return 0;
}

